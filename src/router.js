import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

const Freelancer = {
  Freelancer: () => import(/* webpackChunkName: "freelancer" */ '@/views/freelancer/Freelancer.vue'),
  Navigation: () => import(/* webpackChunkName: "freelancer_navigation" */ '@/views/freelancer/Navigation.vue'),
  Dashboard: () => import(/* webpackChunkName: "freelancer_dashboard" */ '@/views/freelancer/Dashboard.vue'),
  EditProfile: () => import(/* webpackChunkName: "freelancer_edit_profile" */ '@/views/freelancer/EditProfile.vue'),
  EditResume: () => import(/* webpackChunkName: "freelancer_edit_resume" */ '@/views/freelancer/EditResume.vue'),
  AppliedJob: () => import(/* webpackChunkName: "freelancer_applied_job" */ '@/views/freelancer/AppliedJob.vue')
}

const Employer = {
  Employer: () => import(/* webpackChunkName: "employer" */ '@/views/employer/Employer.vue'),
  Navigation: () => import(/* webpackChunkName: "employer_navigation" */ '@/views/employer/Navigation.vue'),
  Dashboard: () => import(/* webpackChunkName: "employer_dashboard" */ '@/views/employer/Dashboard.vue'),
  EditProfile: () => import(/* webpackChunkName: "employer_edit_profile" */ '@/views/employer/EditProfile.vue'),
  ManageJob: () => import(/* webpackChunkName: "employer_manage_job" */ '@/views/employer/ManageJob.vue'),
  ManageFreelancer: () => import(/* webpackChunkName: "employer_manage_freelancer" */ '@/views/employer/ManageFreelancer.vue'),
}

const JobDetail = () => import(/* webpackChunkName: "job_detail" */ '@/views/JobDetail.vue')

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/freelancer',
      name: 'freelancer',
      component: Freelancer.Freelancer,
      children: [
        {
          path: 'navigation',
          name: 'freelancer_navigation',
          alias: '',
          component: Freelancer.Navigation,
          children: [
            {
              path: 'dashboard',
              alias: '',
              name: 'freelancer_dashboard',
              component: Freelancer.Dashboard
            },
            {
              path: 'edit-profile',
              name: 'freelancer_edit_profile',
              component: Freelancer.EditProfile
            },
            {
              path: 'edit-resume',
              name: 'freelancer_edit_resume',
              component: Freelancer.EditResume
            },
            {
              path: 'applied-job',
              name: 'freelancer_applied_job',
              component: Freelancer.AppliedJob
            }
          ]
        }
      ]
    },
    {
      path: '/employer',
      name: 'employer',
      component: Employer.Employer,
      children: [
        {
          path: 'navigation',
          name: 'employer_navigation',
          alias: '',
          component: Employer.Navigation,
          children: [
            {
              path: 'dashboard',
              alias: '',
              name: 'employer_dashboard',
              component: Employer.Dashboard
            },
            {
              path: 'edit-profile',
              name: 'employer_edit_profile',
              component: Employer.EditProfile
            },
            {
              path: 'manage-job',
              name: 'employer_manage_job',
              component: Employer.ManageJob
            },
            {
              path: 'manage-freelancer',
              name: 'employer_manage_freelancer',
              component: Employer.ManageFreelancer
            }
          ]
        },
        {
          path: 'job-detail',
          name: 'employer_job_detail',
          component: JobDetail
        }
      ]
    }
  ]
})
